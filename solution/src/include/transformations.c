#include "transformations.h"
#include <malloc.h>

void image_rotation(struct image* picture)
{
    uint32_t h = (*picture).heigth;
    uint32_t w = (*picture).width;

    //https://tenor.com/ru/view/get-rotated-get-rotated-idiot-rotate-rotation-shark-gif-24511062
    struct pixel* rotated_data = (struct pixel*)malloc(w * h * sizeof(struct pixel));

    if (rotated_data == NULL) {
        fprintf(stderr, "There is not enough memory to complete the rotation\n");
        exit(1);
    }

    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            rotated_data[j * h + (h - 1 - i)] = (*picture).data[i * w + j];
        }
    }

    for (size_t i = 0; i < h; i++) {
        for (size_t j = 0; j < w; j++) {
            (*picture).data[i * w + j] = rotated_data[i * w + j];
        }
    }
    (*picture).heigth = w;
    (*picture).width = h;
    free(rotated_data);
}

