#include "utils.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

bool check(const char* filename)
{
    FILE* file = fopen(filename,"ab+");
    if(!file)
    {
        // fprintf(stderr, "file not exist: %d\n",file);
        fclose(file);
        return true;
    }
    else
    {
        fclose(file);
        return false;
    }
    //works perfect
}

void create_file(char* filename)
{
    FILE* new_file = fopen(filename,"ab+");
    if(!new_file)
    {
        fprintf(stderr, "file not created\n");
    }
    fclose(new_file);
    //works perfect
}
