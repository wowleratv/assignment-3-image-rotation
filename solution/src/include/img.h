#ifndef ABSTRACT_IMAGE
#define ABSTRACT_IMAGE
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel
{
    uint8_t b,g,r;
};

struct image //max abstraction from type of file
{
    uint64_t width, heigth;
    struct pixel* data;
};

#endif
