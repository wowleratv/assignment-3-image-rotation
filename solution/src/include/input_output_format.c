#define BR_ printf("\tbr\n");
#define BR2_ printf("\t\tbr2\n");
#define WRITE(ATTR, TYPE) fwrite(&(output_bmp.ATTR), sizeof(TYPE), 1, writestream)
#include "input_output_format.h"

struct image any_type_reader(const char* filename, enum type image_type)
{
    struct image ans;
    FILE* readstream;
    readstream = fopen(filename,"rb");
    if(!readstream)
    {
        fprintf(stderr, "can't open file");
        exit(1);
    }
    if (image_type == BMP) {
        struct bmp_file_header read_image;
        read_image = read_bmp_header(readstream);
        uint8_t* img = NULL;
        img = read_imageData(readstream, read_image);
        ans = extract_image_data_from_bmp(img, read_image.biHeight, read_image.biWidth);
        free(img);
    }
    else {
        fprintf(stderr, "Unknown image type\n");
        exit(1);
    }
    fclose(readstream);
    return ans;
}

void any_type_writer(struct image* out_img, const char* filename, enum type image_type) {
    FILE* writestream;
    writestream = fopen(filename,"wb");
    if(!writestream)
    {
        fprintf(stderr, "can't open output file");
        exit(1);
    }
    if (image_type == BMP) {
        save_as_bmp(out_img, writestream);
    }
    else {
        fprintf(stderr, "Unknown image type\n");
        exit(1);
    }
    fclose(writestream);
}


