#ifndef _TRANSFORMATIONS_H_
#define _TRANSFORMATIONS_H_
#include "img.h"
#include "inside_format_bmp.h"

void image_rotation(struct image* picture);

#endif
