#ifndef _UTILS_H_
#define _UTILS_H_
#include "img.h"
#include "inside_format_bmp.h"
#include <stdbool.h>

bool check(const char* filename);
void create_file(char* filename);
#endif
