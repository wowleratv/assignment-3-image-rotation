#ifndef _INSIDE_FORMAT_H_
#define _INSIDE_FORMAT_H_
#include "img.h"
#include  <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_file_header 
{
    //54 причина описанна ниже
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;//54 причина описанна ниже
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;//0
        uint16_t biBitCount;// 24 по идее кол-во битов на точку
        uint32_t biCompression;//0 тк мы поддреживаем только 24битные цветовые пространства
        uint32_t biSizeImage;// размер изображения в байтах учитывая padding
        uint32_t biXPelsPerMeter;//0
        uint32_t biYPelsPerMeter;//0
        uint32_t biClrUsed;//0
        uint32_t  biClrImportant;//0
};
#pragma pack(pop)

size_t get_padding(size_t w);

struct bmp_file_header read_bmp_header(FILE* readStream);

uint8_t* read_imageData(FILE* readstream, struct bmp_file_header read_image);

struct image extract_image_data_from_bmp(uint8_t* raw_image, size_t h, size_t w);

void save_as_bmp(struct image* out_img, FILE* writestream);

#endif
