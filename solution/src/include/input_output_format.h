#ifndef _INPUT_FORMAT_H_
#define _INPUT_FORMAT_H_
#include "img.h"
#include "inside_format_bmp.h"
#include "utils.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum type {
    BMP
};

struct image any_type_reader(const char* filename, enum type image_type);

void any_type_writer(struct image* out_img, const char* filename, enum type image_type);

#endif
