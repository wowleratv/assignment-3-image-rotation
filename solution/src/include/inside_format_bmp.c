#include "inside_format_bmp.h"

size_t get_padding(size_t w) {
    size_t ans = (4 - ((w * 3) % 4)) % 4;
    return ans ? ans : 0;
}

struct bmp_file_header read_bmp_header(FILE* readStream) {
    struct bmp_file_header read_image;
    size_t read_result;
    read_result = fread(&read_image, 1, sizeof(struct bmp_file_header), readStream);
    if (read_result < 1) {
        fprintf(stderr, "Something is wrong with the header\n");
        exit(1);
    }
    return read_image;
}

uint8_t* read_imageData(FILE* readstream, struct bmp_file_header read_image) {
    uint8_t* ans = (uint8_t*)malloc(read_image.biSizeImage*sizeof(uint8_t));
    if (ans == NULL) {
        fprintf(stderr, "There is not enough memory to read the image\n");
        exit(1);
    }
    size_t read_result;
    read_result = fseek(readstream, read_image.bOffBits, SEEK_SET);
    if (read_result != 0) {
        free(ans);
        fprintf(stderr, "Something is wrong with the image data\n");
        exit(1);
    }
    read_result = fread(ans, sizeof(uint8_t), read_image.biSizeImage, readstream);
    if (read_result < read_image.biSizeImage) {
        free(ans);
        fprintf(stderr, "Something is wrong with the image data\n");
        exit(1);
    }
    return ans;
}

struct image extract_image_data_from_bmp(uint8_t* raw_image, size_t h, size_t w)
{
    const uint8_t ppr = get_padding(w);
    uint32_t width_wp = w*3 + (uint32_t)ppr;
    struct pixel* image_data = NULL;
    image_data = (struct pixel*)malloc(h * w * sizeof(struct pixel));
    if (image_data == NULL) {
        fprintf(stderr, "There is not enough memory to read the image\n");
        exit(1);
    }
    uint32_t imd_index = 0;
    
    for(uint32_t i = 0; i < h; i++)
    {
        for(uint32_t j = 0; j < w*3; j+=3)
        {
            image_data[imd_index].b = raw_image[i * width_wp + j + 0];
            image_data[imd_index].g = raw_image[i * width_wp + j + 1];
            image_data[imd_index].r = raw_image[i * width_wp + j + 2];
            imd_index++;
        }
    }

    struct image extracted_image;
    extracted_image.data = image_data;
    extracted_image.width = w;
    extracted_image.heigth = h;
    return extracted_image;
}

void save_bmp_header (struct image* out_img, FILE* writestream, uint32_t length) {
    struct bmp_file_header output_bmp;
    output_bmp.bfType = 0x4d42;
    output_bmp.bfReserved = 0;
    output_bmp.bOffBits = 54;
    output_bmp.biSize = 40;
    output_bmp.biWidth = (*out_img).width;
    output_bmp.biHeight = (*out_img).heigth;
    output_bmp.biPlanes = 1;
    output_bmp.biBitCount = 24;
    output_bmp.biCompression = 0;
    output_bmp.biSizeImage = length;
    output_bmp.biXPelsPerMeter = 0;
    output_bmp.biYPelsPerMeter = 0;
    output_bmp.biClrUsed = 0;
    output_bmp.biClrImportant = 0;
    output_bmp.bfileSize = output_bmp.biSize + output_bmp.biSizeImage;
    size_t write_result = fwrite(&output_bmp, 1, sizeof(struct bmp_file_header), writestream);
    if (write_result < sizeof(struct bmp_file_header)) {
        fprintf(stderr, "Error while writing bmp header\n");
        exit(1);
    }
}

void save_as_bmp(struct image* out_img, FILE* writestream)
{
    struct pixel* d = (*out_img).data;

    
    const uint8_t ppr = get_padding((*out_img).width);
    uint32_t length = (*out_img).heigth * ((*out_img).width * 3 + ppr);

    save_bmp_header(out_img, writestream, length);

    
    size_t write_result;
    for (size_t i = 0; i < (*out_img).heigth; i++) {
        write_result = fwrite(d + i * (*out_img).width, 3, out_img->width, writestream);
        if (write_result < out_img->width) {
            fprintf(stderr, "Error while writing bmp data\n");
            exit(1);
        }
        write_result = fseek(writestream, ppr, SEEK_CUR);
        if (write_result) {
            fprintf(stderr, "Error while writing bmp data\n");
            exit(1);
        }
    }
}
