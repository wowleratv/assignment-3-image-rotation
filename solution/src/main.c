#include "include/input_output_format.h"
#include "include/transformations.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc < 3)
    {
        fprintf(stderr, "Wrong count of arguments\n");
        fprintf(stderr, "Syntax: main <input> <output>\n");
        return 1;        
    }
    char* input_filename;
    char* transformed_filename;
    input_filename = argv[1];
    transformed_filename = argv[2];
    
    fprintf(stderr, "Name of file where I will save the transformed image: %s\n", transformed_filename);
    
    if (check(input_filename) == 1)
    {
        fprintf(stderr, "Can`t found file such as %s\n", input_filename);
        return 1;
    }
    if (check(transformed_filename) == 1)
    {
        create_file(transformed_filename);
    }
    struct image rtd_img = any_type_reader(input_filename, BMP);
    image_rotation(&rtd_img);
    any_type_writer(&rtd_img, transformed_filename, BMP);
    free(rtd_img.data);
    return 0;
}
